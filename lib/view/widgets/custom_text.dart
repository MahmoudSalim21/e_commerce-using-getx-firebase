import 'package:flutter/material.dart';

class CostomText extends StatelessWidget {
  final String text;
  final double fontSize;
  final Color color;
  final Alignment alignment;

  CostomText({this.alignment =Alignment.topLeft,  this.text = '', this.fontSize = 16, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment,
      child: Text(
        text,
        style: TextStyle(
          color: color,
          fontSize: fontSize,

        ),
      ),
    );
  }
}
