import 'package:flutter/material.dart';

import '../../constance.dart';
import 'custom_text.dart';

class CustomButtom extends StatelessWidget {
  final String text;
  final VoidCallback  onPressed;

  const CustomButtom({Key? key, this.text='',required this.onPressed}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      padding: EdgeInsets.all(18),
      onPressed: onPressed,
      color: PrimaryColor,
      child: CostomText(
        text: text,
        alignment: Alignment.center,
        color: Colors.white,
      ),
    );
  }
}
