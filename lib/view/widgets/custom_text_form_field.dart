import 'package:flutter/material.dart';
import 'package:get_x_first/view/%20widgets/custom_text.dart';

import '../../constance.dart';

class CustomTextFormField extends StatelessWidget {
 final String text;
 final String hint;
 final Function onSaved;
 final Function validator;

  CustomTextFormField({
    this.text='',
    this.hint='',
    required this.onSaved,
    required this.validator});


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          CostomText(
            text: text,
            fontSize: 14,
            color: Colors.grey.shade900,
          ),
          TextFormField(
            onSaved: onSaved(),
            validator: validator(),
            decoration: InputDecoration(
              hintText: hint,
              hintStyle: TextStyle(
                color: Colors.grey
              ),
              fillColor: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}
