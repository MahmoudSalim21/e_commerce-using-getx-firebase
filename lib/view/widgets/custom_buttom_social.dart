import 'package:flutter/material.dart';

import 'custom_text.dart';

class CustomButtomSocial extends StatelessWidget {
  final String text;
  final String imageName;

  const CustomButtomSocial({Key? key, this.text = '', this.imageName = ''})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.grey.shade100,
      ),
      child: FlatButton(
        onPressed: () {},
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          children: [
            Image.asset(imageName),
            const SizedBox(
              width: 80,
            ),
            CostomText(
              text: text,
            )
          ],
        ),
      ),
    );
  }
}
