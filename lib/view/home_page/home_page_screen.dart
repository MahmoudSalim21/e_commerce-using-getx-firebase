import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_x_first/core/%20view_model/view_model.dart';

class HomePageScreen extends StatelessWidget {
  AuthViewModel viewModel= Get.put(AuthViewModel());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Obx(()  => Text('${viewModel.counter.value}')),
          RaisedButton(
              child: Text('click !'),
              onPressed: (){
            viewModel.increment();
          })
        ],
      ),
    );
  }
}
