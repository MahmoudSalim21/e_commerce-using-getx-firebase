import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get_x_first/view/%20widgets/custom_buttom_social.dart';
import 'package:get_x_first/view/%20widgets/custom_button.dart';
import 'package:get_x_first/view/%20widgets/custom_text.dart';
import 'package:get_x_first/view/%20widgets/custom_text_form_field.dart';
import 'package:get_x_first/view/home_page/home_page_screen.dart';
import 'package:get/get.dart';
import '../../constance.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 50, left: 20, right: 20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CostomText(
                  text: "Welcome",
                  fontSize: 30,
                ),
                CostomText(
                  text: "Sign Up",
                  fontSize: 16,
                  color: PrimaryColor,
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            CostomText(
              text: "Sign in to Continue",
              color: Colors.grey,
              fontSize: 16,
            ),
            const SizedBox(
              height: 30,
            ),
            Column(
              children: [
                CustomTextFormField(
                  onSaved: () {},
                  validator: () {},
                  text: "Email",
                  hint: 'sleem.mz@gmail.com',
                ),
                const SizedBox(height: 40),
                CustomTextFormField(
                  onSaved: () {},
                  validator: () {},
                  text: "password",
                  hint: '********',
                ),
                const SizedBox(height: 10),
                CostomText(
                  text: "Forget Password",
                  alignment: Alignment.topRight,
                  fontSize: 14,
                ),
                const SizedBox(
                  height: 20,
                ),

                CustomButtom(
                  text: 'Sign IN',
                  onPressed: () {
                    Get.to(HomePageScreen());
                  },
                ),
                const SizedBox(
                  height: 40,
                ),
                const CustomButtomSocial(
                  imageName: 'assets/images/facbook.png',
                  text: 'Sign In with Facebook',
                ),
                SizedBox(height: 3,),
                const CustomButtomSocial(
                  imageName: 'assets/images/google.png',
                  text: 'Sign In with Google',
                ),

              ],
            ),
          ],
        ),
      ),
    );
  }
}
